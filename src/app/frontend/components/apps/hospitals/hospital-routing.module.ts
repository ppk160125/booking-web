import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HospitalsComponent} from './hospitals.component'

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forChild([
    { path: '', component: HospitalsComponent }
])],
  exports: [RouterModule]
})
export class HospitalRoutingModule { }
