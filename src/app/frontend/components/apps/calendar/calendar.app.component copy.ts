import { Component, OnInit } from '@angular/core';
import { EventService } from 'src/app/frontend/service/event.service';
// @fullcalendar plugins
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';
import timeGridPlugin from '@fullcalendar/timegrid';
import { CalendarOptions } from '@fullcalendar/core';

@Component({
    templateUrl: './calendar.app.component.html',
    styleUrls: ['./calendar.app.component.scss']
})
export class CalendarAppComponent implements OnInit {

    events: any[] = [];

    today: string = '';

    calendarOptions: CalendarOptions   = {
        initialView: 'dayGridMonth'
    };

    showDialog: boolean = false;

    clickedEvent: any = null;

    dateClicked: boolean = false;

    edit: boolean = false;

    tags: any[] = [];

    view: string = '';

    changedEvent: any;
    data:any = [
    	{
            "id": 1,
    		"title": "Maecenas Pulvinar",
    		"start": "2024-01-10",
			"end": "2024-01-10",
			"description": "eu tincidunt in leo maecenas pulvinar lobortis est phasellus sit amet erat nulla tempus vivamus in felis eu sapien cursus",
			"location": "260 Calypso Terrace",
			"backgroundColor": "#FFB6B6",
			"borderColor": "#FFB6B6",
			"textColor": "#212121",
			"tag": {"color": "#FFB6B6", "name": "Company A"}
    	},
    	{
            "id": 2,
    		"title": "Aenean Lectus",
    		"start": "2024-01-10",
			"end": "2024-01-10",
			"description": "Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.",
			"location": "93529 Sloan Junction",
			"backgroundColor": "#FFC7E8",
			"borderColor": "#FFC7E8",
			"textColor": "#212121",
			"tag": {"color": "#FFC7E8", "name": "Company B"}
    	},
    	{
    		"id": 3,
    		"title": "Nam Ultrices",
    		"start": "2024-01-11",
			"end": "2024-01-11",
			"description": "Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.",
			"location": "9295 Almo Avenue",
			"backgroundColor": "#D2D6FF",
			"borderColor": "#D2D6FF",
			"textColor": "#212121",
			"tag": {"color": "#D2D6FF", "name": "Company C"}
    	},
    ]

    constructor(private eventService: EventService) { }

    ngOnInit(): void {
        this.today = '2024-01-10';

        // this.eventService.getEvents().then(events => {
        //     this.events = events;
        //     this.calendarOptions = { ...this.calendarOptions, ...{ events: this.data } };
        //     this.tags = this.events.map(item => item.tag);


        // console.log(this.tags);
        // console.log(this.events);

        // });
        // this.calendarOptions = { ...this.calendarOptions, ...{ events: this.data } };

        console.log(this.tags);

        this.calendarOptions = {
            plugins: [dayGridPlugin, timeGridPlugin, interactionPlugin],
            height: 720,
            initialDate: this.today,
            headerToolbar: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth,timeGridWeek,timeGridDay'
            },
            editable: true,
            selectable: true,
            selectMirror: true,
            dayMaxEvents: true,
            events:this.data,
            selectLongPressDelay:100, //ค่าเร่ิมต้น สำหรับ teblet mobile  = 1000ms  คือต้องกดค้าง 1000 ms
            eventClick: (e: any) => this.onEventClick(e),
            select: (e: any) => this.onDateSelect(e),
            dayCellContent: this.handleDayRender.bind(this),
 
          
        };
    }
    handleDayRender(arg: any) {
        console.log(arg);

        // Check if there is an event for the current date
        const hasEvent = this.data.some(
          (event:any) => event.start === arg.date.toISOString().split('T')[0]
        );
    
        // If no event, disable the day
        if (!hasEvent && arg.el) {
            console.log(arg);
          arg.el.classList.add('fc-day-disabled');
        // console.log('no event');
        }
      }

      isHoliday(date: Date): boolean {
        // Implement your logic to determine if the date is a holiday
        // Replace this with your own holiday detection algorithm
        // For example, you might have a list of holiday dates to check against
        const holidayDates = ['2024-01-10'];
        return holidayDates.some((holiday) => date.toISOString().split('T')[0] === holiday);
      }
    

    onEventClick(e: any) {
        this.clickedEvent = e.event;
        let plainEvent = e.event.toPlainObject({ collapseExtendedProps: true, collapseColor: true });
        this.view = 'display';
        this.showDialog = true;

        this.changedEvent = { ...plainEvent, ...this.clickedEvent };
        this.changedEvent.start = this.clickedEvent.start;
        this.changedEvent.end = this.clickedEvent.end ? this.clickedEvent.end : this.clickedEvent.start;
    }

    onDateSelect(e: any) {
        console.log(e);
        if(e.start < new Date()){
            console.log('date is less today');
        }else{
            this.view = 'new'
            this.showDialog = true;
            this.changedEvent = { ...e, title: null, description: null, location: null, backgroundColor: null, borderColor: null, textColor: null, tag: { color: null, name: null } };
        }
     
    }

    handleSave() {
        if (!this.validate()) {
            return;
        }
        else {
            this.showDialog = false;
            this.clickedEvent = { ...this.changedEvent, backgroundColor: this.changedEvent.tag.color, borderColor: this.changedEvent.tag.color, textColor: '#212121' };

            if (this.clickedEvent.hasOwnProperty('id')) {
                this.events = this.events.map(i => i.id.toString() === this.clickedEvent.id.toString() ? i = this.clickedEvent : i);
            } else {
                this.events = [...this.events, { ...this.clickedEvent, id: Math.floor(Math.random() * 10000) }];
            }
            this.calendarOptions = { ...this.calendarOptions, ...{ events: this.events } };
            this.clickedEvent = null;
        }

    }

    onEditClick() {
        this.view = 'edit';
    }

    delete() {
        this.events = this.events.filter(i => i.id.toString() !== this.clickedEvent.id.toString());
        this.calendarOptions = { ...this.calendarOptions, ...{ events: this.events } };
        this.showDialog = false;
    }

    validate() {
        let { start, end } = this.changedEvent;
        return start && end;
    }
 

}
