import { Component, OnInit } from '@angular/core';

@Component({
    templateUrl: './faq.component.html'
})
export class FaqComponent implements OnInit {

    items: any[] = [];

    activeIndex: number = 0;

    constructor() { }

    ngOnInit(): void {
        this.items = [{ 
                label: 'ทั่วไป', icon: 'pi pi-fw pi-info-circle', questions: [
                    'ใครใช้ได้บ้าง?', 'Do I need to sign up with credit card?', 'Is the subscription monthly or annual?', 'How many tiers are there?'
                ] 
            },
            { 
                label: 'การจอง', icon: 'pi pi-fw pi-envelope', questions: [
                    'จะได้นัดเมื่อไหร่?', 'มีบัตรคิวไหม?', 'ตรวจสอบคิวได้อย่างไร?', 'ต้องเตรียมอะไรบ้าง?'
                ] 
            },
            { 
                label: 'การใช้งาน', icon: 'pi pi-fw pi-question-circle', questions: [
                    'How can I get support?', 'What is the response time?', 'Is there a community forum?', 'Is live chat available?'
                ] 
            },
        ];
    };

    changeItem(i: number) {
        this.activeIndex = i;
    }
}
